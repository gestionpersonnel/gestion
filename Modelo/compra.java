package Modelo;
public class compra {
	private String produto;
	private double valor;
	private int id;
	private int id_usuario;
	private int id_cartao;
	
	public int getId_cartao() {
		return id_cartao;
	}
	public void setId_cartao(int id_cartao) {
		this.id_cartao = id_cartao;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	
}
