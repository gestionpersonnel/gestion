package Modelo;
public class conta {
	private String numero;
	private Double valor;
	private int id;
	private int id_usuario;
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double d) {
		this.valor = d;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
