package controle;
import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Usuario;
import Modelo.conta;
public class ControleConta {
	public boolean atualizarValor(conta conta) throws SQLException {
		boolean resu = false;
		Connection con = new Conexao().abrirConexao();
		String slq = "UPDATE conta SET valor=? WHERE id_usuario=?";
		PreparedStatement ps = con.prepareStatement(slq);
		ps.setDouble(1, conta.getValor());
		ps.setInt(2, conta.getId_usuario());
		if(!ps.execute()){
			resu = true;
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		new Conexao().fecharConexao(con);	
		return resu;
	}
	public boolean inserir(conta co){
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO conta(numero,valor,id_usuario) VALUES(?,?,?); ");
			ps.setString(1, co.getNumero());
			ps.setDouble(2, co.getValor());
			ps.setInt(3, co.getId_usuario()); 
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public boolean atualizar(conta co) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("UPDATE conta SET numero=?, valor=? WHERE id=?");
		ps.setString(1, co.getNumero());
		ps.setDouble(2, co.getValor());
		ps.setInt(3, co.getId());
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM conta WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	public conta consultar(int id_usuario,HttpServletRequest request) throws SQLException{
		conta co = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE id_usuario=?;");
		ps.setInt(1,id_usuario);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			co = new conta();
			co.setId(rs.getInt("id"));
			co.setNumero(rs.getString("nome"));
			co.setValor(rs.getFloat("valor"));
			Usuario user = (Usuario)request.getSession().getAttribute("usuario");
			co.setId_usuario(user.getId());
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return co;

	}
	public ArrayList<conta> consultarTodos() throws SQLException{
		ArrayList<conta> lista = null;		
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM conta;");
		ResultSet rs = ps.executeQuery();
		if(rs != null){
			lista = new ArrayList<conta>();
			while(rs.next()) {
				conta co = new conta();
				co.setNumero(rs.getString("numero"));
				co.setValor(rs.getFloat("valor"));
				co.setId(rs.getInt("id"));
				lista.add(co);
			}
		}
		new Conexao().fecharConexao(con);
		return lista;

	}
	public conta consultarNumeroConta(String Nconta) throws SQLException{
		conta cont = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM conta WHERE numero=?;");
		ps.setString(1,Nconta);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			conta co = new conta();
			co.setId(rs.getInt("id"));
			co.setNumero(rs.getString("numero"));
			co.setValor(rs.getFloat("valor"));
			new Conexao().fecharConexao(con);
			cont=co;
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return cont;

	}
	public conta consultarValor(int id_usuario) throws SQLException{		
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement(" SELECT valor  FROM conta WHERE id_usuario=?;");
		conta conta = null;
		ps.setInt(1,id_usuario);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			conta = new conta();
			conta.setValor(rs.getFloat("valor"));
			conta.setId_usuario(id_usuario);
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return conta;

	}
	public conta consultarId(int id_usuario) throws SQLException{
		conta co = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT id FROM conta WHERE id_usuario=?;");
		ps.setInt(1,id_usuario);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			co =new conta();
			co.setId(rs.getInt("id"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao consultar as informações.");
		}
				
		return co;

	}
}
