package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.depositar;
public class ControleDepo {
	public boolean inserir(depositar dep){
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO depositar(valor,data,id_usuario,id_conta) VALUES(?,?,?,?);");
			ps.setDouble(1, dep.getValor());
			ps.setString(2, dep.getData());
			ps.setInt(3, dep.getId_usuario());
			ps.setInt(4, dep.getId_conta());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM depositar WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	public ArrayList<depositar> consultarTodos() throws SQLException{
		ArrayList<depositar> lista = null;		
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM depositar ;");
		ResultSet rs = ps.executeQuery();
		if(rs != null){
			lista = new ArrayList<depositar>();
			while(rs.next()) {
				depositar dep = new depositar();
				dep.setValor(rs.getFloat("valor"));
				dep.setData(rs.getString("data"));
				dep.setId(rs.getInt("id"));
				lista.add(dep);
			}
		}
		new Conexao().fecharConexao(con);
		return lista;
	}
}
