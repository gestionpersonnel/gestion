package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.cartao;
import Modelo.conta;
public class ControleCartao {
public class ControleCartao {
	public cartao Nome(String nome, int id_usuario ) throws SQLException{
		cartao car = new cartao();
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT nome FROM cartao WHERE nome=? AND id_usuario=?");
		ps.setString(1, nome);
		ps.setInt(2, id_usuario);
		ResultSet rs = ps.executeQuery();
		if(rs.next() && rs != null){
			car.setNome(rs.getString("nome"));
		}else{
			car=null;
		}
		new Conexao().fecharConexao(con);	
		return car;
	}
	public boolean inserir(cartao car){
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO cartao(numero,nome,id_usuario) VALUES(?,?,?);");			
			ps.setString(1, car.getNumero());
			ps.setString(2, car.getNome());			
			ps.setInt(3, car.getId_usuario());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public boolean atualizar(cartao car) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("UPDATE cartao SET numero=?, nome=? WHERE id=?");
		ps.setString(1, car.getNumero());
		ps.setString(2, car.getNome());
		ps.setInt(3, car.getId());
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM conta WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
    public cartao consultar(int id_usuario, String nome) throws SQLException{
    		cartao car = null;
    		Connection con = new Conexao().abrirConexao();
    		PreparedStatement ps = con.prepareStatement("SELECT * FROM cartao WHERE id_usuario=? AND nome=?;");
    		ps.setInt(1,id_usuario);
    		ps.setString(2, nome);
    		ResultSet rs = ps.executeQuery();
    		if(rs != null && rs.next()){
    			car = new cartao();
    			car.setNumero(rs.getString("numero"));
    			car.setId(rs.getInt("id"));
    			car.setNome(rs.getString("nome"));
    			new Conexao().fecharConexao(con);
    		}else {
    			throw new SQLException("Erro ao inserir as informações.");
    		}
    				
    		return car;
    
    	}
	public ArrayList<cartao> consultarTodos(int id_usuario) throws SQLException{
		ArrayList<cartao> lista = null;		
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM cartao WHERE id_usuario=?;");
		cartao car = new cartao();
		ps.setInt(1, car.getId_usuario());
		ResultSet rs = ps.executeQuery();
		if(rs != null){
			lista = new ArrayList<cartao>();
			while(rs.next()) {
				car.setNumero(rs.getString("numero"));
				car.setNome(rs.getString("nome"));
				car.setId(rs.getInt("id"));
				lista.add(car);
			}
		}
		new Conexao().fecharConexao(con);
		return lista;
	}
	public cartao consultarId(int id_usuario) throws SQLException{
		cartao ca = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT id FROM cartao WHERE id_usuario=?;");
		ps.setInt(1,id_usuario);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			ca =new cartao();
			ca.setId(rs.getInt("id"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao consultar as informações.");
		}
				
		return ca;

	}
		public cartao consultarNome(String nome) throws SQLException{
		cartao car = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM cartao WHERE nome=? AND id_usuario;");
		car = new cartao();
		ps.setString(1,nome);
		ps.setInt(2, car.getId_usuario());
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			car.setNumero(rs.getString("numero"));
			car.setNome(rs.getString("nome"));
			car.setId(rs.getInt("id"));
			car.setId_usuario(rs.getInt("id_usuario"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return car;

	}
}
