<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="Includes/header.jsp"></c:import>
<body>
<div id="img-bk">
	<div class="uk-position-center uk-overlay uk-overlay-default">
		<div class="uk-card uk-card-default uk-card-body ">
			<form action="/Clemilton/recebendoCad" method="post">
				<div class="uk-margin">
			        <div class="uk-inline">
			            <span class="uk-form-icon" uk-icon="icon: user"></span>
							<input class="uk-input uk-form-width-medium" type="text" name="nome"  required/>
						</div>
				</div>
				<div class="uk-margin">
			        <div class="uk-inline">
			            <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
						<input class="uk-input uk-form-width-medium" type="password" name="senha"  required/>
					</div>
				</div>
				<div class="uk-margin">
			        <div class="uk-inline">
			            <span class="uk-form-icon" uk-icon="icon: mail"></span>
						<input class="uk-input uk-form-width-medium" type="email" name="email" required placeholder="maria@gmail"/>
					</div>
				</div>
				<div class="uk-margin">
			        	<div class="uk-inline">            					
							<select class="uk-select uk-form-width-medium" name="tipConta">
								 <option value=""></option>
								  <option value="cor">Corrente</option>
								   <option value="pou">Poupan�a</option>     
							</select><br>
						</div>
					</div>	
				    <hr class="uk-divider-icon">		
				<center><button  class="uk-button uk-button-default">enviar</button></center>
			</form>
		</div>
	</div>
</div>
<c:import url="Includes/footer.jsp"></c:import>