<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="Modelo.Usuario" %>
<%@ page import="controle.ControleUsuario" %>
<%@ page import="java.util.ArrayList"%>
<% 
	Usuario user = new Usuario();	 
%>
<c:import url="Includes/header.jsp"></c:import>
<body>
<div id="img-bk">
	<div class="uk-position-center uk-overlay uk-overlay-default">
		<div class="uk-card uk-card-default uk-card-body ">
		<form action="editarUser" method="post">
			<label>Editar usu�rio</label>
			<div class="uk-margin">
        		<div class="uk-inline">
            		<span class="uk-form-icon" uk-icon="icon: user"></span>
					<input class="uk-input uk-form-width-medium" type="text" name="nome"  required/>
				</div>
			</div>
			<div class="uk-margin">
        		<div class="uk-inline">
		            <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
					<input class="uk-input uk-form-width-medium" type="password" name="senha"  required/>
				</div>
			</div>
			<div class="uk-margin">
		        <div class="uk-inline">
		            <span class="uk-form-icon" uk-icon="icon: mail"></span>
					<input class="uk-input uk-form-width-medium" type="email" name="email" required placeholder="maria@gmail"/>
				</div>
			</div>
			<input type="hidden" name="id" value='<%=request.getParameter("id") %>'>
			 <hr class="uk-divider-icon">		
	<center><button  class="uk-button uk-button-default">enviar</button></center>
		</form>
		</div>
	</div>
</div>
</body>
</html>