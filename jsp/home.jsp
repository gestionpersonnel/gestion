<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="Modelo.Usuario" %>
<%@ page import="controle.ControleUsuario" %>
<%@ page import="java.util.ArrayList"%>
<c:import url="Includes/header.jsp"></c:import>
<body>
<%
if(session != null){
%>
<div id="bot"></div>
<div id="header">
	<div class='uk-position-top-center'>
    	<img src='ft3.png' style='width:100%;' alt=''>
    	<div class='uk-position-bottom-left'>
        <nav class='uk-navbar-container uk-navbar-transparent' uk-navbar>
            <div class='uk-navbar-left'>
             <ul class="uk-navbar-nav">
             <li>
             <a href="#"><p>Menu</p></a>
                <div class="uk-navbar-dropdown uk-navbar-dropdown-width-2">
                    <div class="uk-navbar-dropdown-grid uk-child-width-1-2" uk-grid>
                        <div>
                			<ul class='uk-nav uk-navbar-dropdown-nav'>
                   			<li class="uk-active"><a href="home.jsp">Home</a></li>
					        <li ><a href="compras.jsp">Adicionar compras</a></li>
					        <li ><a href="ativ.jsp">Minhas compras</a></li>					        
                			</ul>
            			</div>
            			<div>
	            			<ul class='uk-nav uk-navbar-dropdown-nav'>	            					            			
					        	<li><a href="#">Adicionar cart�o</a>
						        	<div uk-dropdown="pos: right-center" class="uk-navbar-dropdown">
						        		<ul class="uk-nav uk-navbar-dropdown-nav">
						        			<li><a href="credito.jsp">Cr�dito</a></li>
						        			<li><a href="debito.jsp">D�bito</a></li>
						        		</ul>
						        	</div>
					        	</li>
	            				<li><a href="saque.jsp">Saques</a></li>
					        	<li><a href="deposito.jsp">Dep�sitos</a></li>
								<li><a href="deslogar.jsp">SAIR</a></li>
	            			</ul>
            			</div>
            		</div>
            	</div>
            </li>
            </ul>
           </div>
        </nav>
    </div>
 	</div>
 </div>

<footer>
	<div class="uk-column-1-2 uk-column-divider">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>

    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>    
</div>
</footer>
<%
}else{
	response.sendRedirect("login.jsp");
}
%>
</body>
<script>
$('#bot').ready(function(){
	Swal.fire({
		  position: 'top-end',
		  type: 'success',
		  title: 'Bem-vindo',
		  showConfirmButton: false,
		  timer: 1500
		})
});
</script>
</html>

