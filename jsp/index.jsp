<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1"  content="width=device-width, initial-scale=1">
	<title>Gerenciamento pessoal</title>
	<link rel="stylesheet" href="css/uikit.css">
	<script src="js/uikit.js"></script>
	<script src="js/uikit-icons.js"></script>		
	<link rel="stylesheet" type="text/css" href="index.css">	
</head>
<body>
<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="ratio: 8:4; animation: push;autoplay: true" >
    <ul class="uk-slideshow-items">
        <li>
           <img src="vdc2.jpg" uk-cover  uk-img="target: !.uk-slideshow-items">
             <div class="uk-position-top">             
             	<div class="uk-position-relative">    
    				<div class="uk-position-top-left">
        				<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
				            <div class="uk-navbar-left">
                				<ul class="uk-navbar-nav">
                    				<li class="uk-active"><a href="login.jsp">Login</a></li>
                    				<li class="uk-active"><a href="cadastro.jsp">Cadastro</a></li>					           		
                				</ul>
            				</div>
        				</nav>
	    			</div>
				</div>
             </div>	
             <div class="uk-position-center-left">
             <div id="leftt">             
             <h1>Maneira mais simples de <br>gerenciar suas <br>finan�as pessoais.</h1>
			<h3>Mais de 5 milh�es de pessoas escolheram <br>nosso aplicativo para 
			gerenciar seus gastos <br>e n�o tomar susto ao final do m�s.</h3>	             	
             </div>
             </div>
        </li>
        <li>
           <img src="sa.jpg" alt="" uk-cover  uk-img="target: !.uk-slideshow-items">
            	<div class="uk-position-top">             
             	<div class="uk-position-relative">    
    				<div class="uk-position-top-left">
        				<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
				            <div class="uk-navbar-left">
                				<ul class="uk-navbar-nav">
                    				<li class="uk-active"><a href="login.jsp">Login</a></li>
                    				<li class="uk-active"><a href="cadastro.jsp">Cadastro</a></li>					           		
                				</ul>
            				</div>
        				</nav>
	    			</div>
				</div>
             </div>	
            	<div class="uk-position-center-left">
            		<div class="uk-child-width-1-2@s" uk-grid>
            			<div>
		        			<div class="uk-card uk-card uk-card-small uk-card-body">
		        				<div id="leftt">
			        				<h1>Gestion Personnel</h1>
			        				<p><h3>Aqui voc� encontra a melhor gest�o financeira 
			        				para facilitar sua vida</h3>
		        				</div>
		        			</div>
        				</div>
        			</div>
        		</div>
        </li>
        <li>
            <img src="64857.jpg" alt="" uk-cover  uk-img="target: !.uk-slideshow-items"> 
           		<div class="uk-position-top">             
             	<div class="uk-position-relative">    
    				<div class="uk-position-top-left">
        				<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
				            <div class="uk-navbar-left">
                				<ul class="uk-navbar-nav">
                    				<li class="uk-active"><a href="login.jsp">Login</a></li>
                    				<li class="uk-active"><a href="cadastro.jsp">Cadastro</a></li>					           		
                				</ul>
            				</div>
        				</nav>
	    			</div>
				</div>
             </div>	
            <div class="uk-position-center-left">
	            <div class="uk-child-width-1-2@s" uk-grid>
	            	<div>
	        			<div class="uk-card uk-card uk-card-small uk-card-body">
		            		<div id="left">		            		
		            			<h1>Parcerias externas</h1>
		            			<p><h3>Caixa<br>Bradesco<br>Visa<br>Mastercard</h3>
		            		</div>
		        		</div>
	    			</div>
	            </div>
            </div>           
        </li>
    </ul>

    <a class="uk-slidenav-large uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
    <a class="uk-slidenav-large uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

</div>
</body>
</html>