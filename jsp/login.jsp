<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="Includes/header.jsp"></c:import>
<body>
<%
	if(session == null){		
%>
	<div id="img-bk">
	
		<div class="uk-position-center uk-overlay uk-overlay-default">
			<div class="uk-card uk-card-default uk-card-body ">
				<form action="receberLogin" method="post">
					<div class="uk-margin">
        				<div class="uk-inline">
            				<span class="uk-form-icon" uk-icon="icon: user"></span>
							<input class="uk-input uk-form-width-medium" type="text" name="nome"  required/>
						</div>
					</div>
					<div class="uk-margin">
        				<div class="uk-inline">
            				<span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
							<input class="uk-input uk-form-width-medium" type="password" name="senha"  required/>
						</div>
					</div>					
					<hr class="uk-divider-icon">		
					<center>
						<button  class="uk-button uk-button-default">enviar</button><br>
						<a href="cadastro.jsp">cadastrar-se</a>
					</center>							 
				</form>				
			</div>
		</div>
	</div>
	<%
	}else{
		response.sendRedirect("home.jsp");
	}
	%>
</body>
</html>