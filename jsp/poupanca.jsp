<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="Includes/header.jsp"></c:import>
<body>
<div id="img-bk">
	<div class="uk-position-center uk-overlay uk-overlay-default">
		<div class="uk-card uk-card-default uk-card-body ">
			<form action="/Clemilton/recebendoPou" method="post">		
				<div class="uk-margin">
		        	<div class="uk-inline">
		            	<label>Numero da Conta</label> 
							<input class="uk-input uk-form-width-medium" type="text" name="nconta" placeholder="123.456.789-00">
					</div>
				</div>
				<div class="uk-margin">
		        	<div class="uk-inline">
		            	<label>Saldo da conta</label>		
							<input class="uk-input uk-form-width-medium" type="text" name="valor">
					</div>
				</div>				
				<hr class="uk-divider-icon">
				<center><button class="uk-button uk-button-default">Enviar</button></center>
			</form>
		</div>
	</div>
</div>
<c:import url="Includes/footer.jsp"></c:import>
