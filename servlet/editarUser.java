import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Modelo.Usuario;
import controle.ControleUsuario;

@WebServlet("/editarUser")
public class editarUser extends HttpServlet {
	private static final long serialVersionUID = 1L; 	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Usuario user = new Usuario();
			ControleUsuario con = new ControleUsuario();
			user.setNome(request.getParameter("nome"));
			user.setSenha(request.getParameter("senha"));
			user.setEmail(request.getParameter("email"));
			int id = Integer.parseInt(request.getParameter("id"));
			con.atualizar(user,id);
		}catch(Exception e) {
			e.getMessage();
		}
	}
}
