import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.Usuario;
import controle.ControleUsuario;

@WebServlet("/recebendoCad")
public class recebendoCad extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			ControleUsuario con = new ControleUsuario();
			Usuario user = new Usuario();
			user.setNome(request.getParameter("nome"));
			user.setSenha(request.getParameter("senha"));
			user.setEmail(request.getParameter("email"));
			con.inserir(user);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		switch (request.getParameter("tipConta")) {
		case "cor":
			response.sendRedirect("corrente.jsp");					
			break;

		case "pou":
			response.sendRedirect("poupanca.jsp");
			break;					
		}
	}
}
