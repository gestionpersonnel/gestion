import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.Corrente;
import Modelo.Usuario;
import Modelo.conta;
import controle.ControleConta;
import controle.ControleCorrente;
@WebServlet("/recebendoCor")
public class recebendoCor extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		conta con = new conta();
		ControleConta ccon =new ControleConta();
		Corrente cor = new Corrente();		
		ControleCorrente ccor = new ControleCorrente();
		Usuario user = (Usuario)request.getSession().getAttribute("usuario");			
			con.setNumero(request.getParameter("numero"));
			con.setValor(Float.parseFloat(request.getParameter("valor")));
			con.setId_usuario(user.getId());
			if(ccon.inserir(con)){
				try {
					int idConta = ccon.consultarNumeroConta(con.getNumero()).getId();
					cor.setTaxa(Float.parseFloat(request.getParameter("taxa")));
					cor.setId_conta(idConta);
					if(ccor.inserir(cor)) {
						response.sendRedirect("home.jsp");
					}else {
						response.getWriter().print("conta nao criada");
					}
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}	
			}else {
				response.sendRedirect("cadastro.jsp");
			}
		}
}
