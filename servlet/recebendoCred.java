import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import controle.ControleCredito;
import Modelo.Credito;
import Modelo.Usuario;
import Modelo.cartao;
import controle.ControleCartao;
@WebServlet("/recebendoCred")
public class recebendoCred extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario user = (Usuario)request.getSession().getAttribute("usuario");
		if(user != null) {
			ControleCredito ccred = new ControleCredito();
			Credito cred = new Credito();
			cartao car =new cartao();
			ControleCartao ccar= new ControleCartao();
			cred.setLimite(Double.parseDouble(request.getParameter("limite")));
			cred.setPrazo(request.getParameter("prazo"));
			car.setNumero(request.getParameter("numero"));
			car.setId_usuario(user.getId());
			car.setNome(request.getParameter("nome"));
			try {
				String campNome = car.getNome();
				cartao Nome =ccar.Nome(campNome, user.getId());
				if(Nome == null) {
					if(ccar.inserir(car)) {
						int idCartao = ccar.consultar(car.getId_usuario(), car.getNome()).getId();
						cred.setId_cartao(idCartao);
						cred.setId_usuario(user.getId());
					
					}else {
						response.getWriter().print("cartao n inserido");	
					}
				}else{
					response.getWriter().print("Nome já existe");
					}		
				if(ccred.inserir(cred)) {
					response.sendRedirect("credito.jsp");
				}else{
					response.getWriter().print("credito n inserido");
				}
			}catch (SQLException e) {
						System.out.println(e.getMessage());
			}
		}
	}
}



