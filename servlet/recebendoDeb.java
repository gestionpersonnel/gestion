import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.Debito;
import controle.ControleDebito;
import Modelo.Usuario;
import Modelo.cartao;
import controle.ControleCartao;
@WebServlet("/recebendoDeb")
public class recebendoDeb extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario user = (Usuario)request.getSession().getAttribute("usuario");
		if(user != null) {
			ControleCartao ccar = new ControleCartao();
			cartao car = new cartao();
			Debito deb = new Debito();
			ControleDebito cdeb = new ControleDebito();
			car.setNumero(request.getParameter("numero"));
			car.setNome(request.getParameter("nome"));
			car.setId_usuario(user.getId());
			try {
				String campNome = car.getNome();
				cartao Nome =ccar.Nome(campNome, user.getId());
				if(Nome == null) {
					if(ccar.inserir(car)) {
						int idCartao = ccar.consultar(car.getId_usuario(), car.getNome()).getId();
						deb.setId_cartao(idCartao);
						deb.setId_usuario(user.getId());
							if(cdeb.inserir(deb)){
								response.sendRedirect("debito.jsp");
							}else{
								response.getWriter().print("debito n inserido");
								}
					}else {
						response.getWriter().print("cartao n inserido");	
					}
				}else{
					response.getWriter().print("Nome já existe");
					}	
			}catch (SQLException e) {
						System.out.println(e.getMessage());
			}
		}
	}
}