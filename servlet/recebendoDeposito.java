import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.depositar;
import controle.ControleDepo;
import controle.ControleConta;
import Modelo.Usuario;
import Modelo.conta;
@WebServlet("/recebendoDeposito")
public class recebendoDeposito extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControleConta cco = new ControleConta();
		ControleDepo con = new ControleDepo();
		depositar dep = new depositar();
		conta co = new conta();
		try {
			Usuario user = (Usuario)request.getSession().getAttribute("usuario");
			int idConta = cco.consultarId(user.getId()).getId();
			dep.setId_usuario(user.getId());
			dep.setId_conta(idConta);
			dep.setData(request.getParameter("data"));
			dep.setValor(Double.parseDouble(request.getParameter("valor")));
			double valorConta = cco.consultarValor(user.getId()).getValor();
			double valor = Double.parseDouble(request.getParameter("valor"));
			co.setValor(valorConta + valor);
			co.setId_usuario(user.getId());
				if(cco.atualizarValor(co)){					
					con.inserir(dep);
					response.sendRedirect("saque.jsp");
				}else {
					response.sendRedirect("saque.jsp");
				}			
			}catch(Exception e){
				System.out.println(e.getMessage()); 
		}

}
}