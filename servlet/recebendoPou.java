import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.conta;
import controle.ControleConta;
import controle.ControlePoupanca;
import Modelo.Poupanca;
import Modelo.Usuario;
@WebServlet("/recebendoPou")
public class recebendoPou extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			conta co = new conta();
			ControleConta con= new ControleConta();
			ControlePoupanca cpou = new ControlePoupanca();
			Poupanca pou = new Poupanca();
			Usuario user = (Usuario)request.getSession().getAttribute("usuario");
			co.setNumero(request.getParameter("numero"));
			co.setValor(Float.parseFloat(request.getParameter("valor")));
			co.setId_usuario(user.getId());
			if(con.inserir(co)) {
				try {
					int idConta = con.consultarNumeroConta(co.getNumero()).getId();
					pou.setId_conta(idConta);
					if(cpou.inserir(pou)) {
						response.sendRedirect("home.jsp");
					}else {
						response.getWriter().print("conta nao criada");
					}
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}	
			}else {
				response.sendRedirect("cadastro.jsp");
			}

		
	}
}
