import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.sacar;
import controle.ControleSacar;
import Modelo.Usuario;
import Modelo.conta;
import controle.ControleConta;
@WebServlet("/recebendoSaque")
public class recebendoSaque extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario user = (Usuario)request.getSession().getAttribute("usuario");
		if(user != null) {
			ControleSacar con = new ControleSacar();
			sacar sac = new sacar();
			conta co = new conta();
			ControleConta cco = new ControleConta();			
				try {
					int idConta = cco.consultarId(user.getId()).getId();
					sac.setId_usuario(user.getId());				
					sac.setId_conta(idConta);
					sac.setData(request.getParameter("data"));
					sac.setValor(Double.parseDouble(request.getParameter("valor")));
					double valorConta = cco.consultarValor(user.getId()).getValor();
					double valor = Double.parseDouble(request.getParameter("valor"));
						if(valor <= valorConta){					
							co.setValor(valorConta - valor);
							co.setId_usuario(user.getId());
							if(cco.atualizarValor(co)) {
								if(con.inserir(sac)) {
									response.sendRedirect("saque.jsp");
								}else {
									response.sendRedirect("saque.jsp");
								}
							}
						}else {
							response.getWriter().print("ur");
						}
				}catch(SQLException e){
					System.out.println(e.getMessage());		
				}
		}
	}
}