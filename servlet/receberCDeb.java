

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.Usuario;
import Modelo.cartao;
import Modelo.compra;
import Modelo.conta;
import controle.ControleCompra;
import controle.ControleConta;
import controle.ControleCartao;
@WebServlet("/receberCDeb")
public class receberCDeb extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		compra co = new compra();
		ControleCompra cco = new ControleCompra();
		conta con = new conta();
		cartao car = new cartao();
		ControleConta ccon = new ControleConta();
		ControleCartao ccar = new ControleCartao();
		co.setProduto(request.getParameter("produto"));
		co.setValor(Double.parseDouble(request.getParameter("valor")));
		car.setNome(request.getParameter("cartao"));
		try {
			Usuario user = (Usuario)request.getSession().getAttribute("usuario");
			double valor =co.getValor();
			 double contaValor = ccon.consultarValor(user.getId()).getValor();
			 int idCartao =ccar.consultarNome(car.getNome()).getId();
			 car.setId_usuario(user.getId());
			 String campNome = car.getNome();
			 cartao Nome = ccar.Nome(campNome,car.getId_usuario());
			 if(Nome!=null){
				if(valor <= contaValor){
					con.setValor(contaValor - valor);
					con.setId_usuario(user.getId());
					if(ccon.atualizarValor(con)){
						co.setId_usuario(user.getId());
						co.setId_cartao(idCartao);
						if(cco.inserir(co)) {
							response.sendRedirect("comprasDeb.jsp");
						}else {
							response.getWriter().print("compra n inserida");	
						}
					}else {
						response.getWriter().print("valor n atualizada");	
					}
				}else {
					response.getWriter().print("valor maior do que o valor da conta");	
				}
			 }else{
				 response.getWriter().print("cartao inv�lido");
			 }
		}catch(Exception e){
			e.getMessage();
		}
	}

}
