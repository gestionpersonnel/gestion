import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Modelo.Usuario;
import controle.ControleUsuario;
@WebServlet("/receberLogin")
public class receberLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Usuario usuario  = new Usuario();
			ControleUsuario con = new ControleUsuario();
			usuario.setNome(request.getParameter("nome"));
			usuario.setSenha(request.getParameter("senha"));
			Usuario rec = con.logar(usuario);
			if(rec!=null) {
				HttpSession session = request.getSession();
				session.setAttribute("rec", rec);
				response.sendRedirect("home.jsp");
			}else{				
				response.sendRedirect("login.jsp");
			}
		}catch(Exception e) {
			e.getMessage();
		}
	}

}
